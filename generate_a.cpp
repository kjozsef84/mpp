#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <iomanip>

using namespace std;

int main (int argc, char *argv[])
{
    int iSecret, iGuess, n;
    n = atoi(argv[1]);

    /* initialize random seed: */
    srand (time(NULL));
    int row = n;    
    for (int i = 0; i < n; i++)
    {

        for (int j = 0; j < row; j++){
            /* generate secret number between 1 and 10: */
            float r2 = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/2));
            cout << std::fixed << std::setprecision(2);
            cout << r2 << " ";
        }
        cout << endl;
        row--;
    }
    cout << endl;
    return 0;
}