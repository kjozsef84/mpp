#include <iostream>
#include <vector>
#include <fstream>
#include <chrono> 

#include "input_output.h"

using namespace std;
using namespace std::chrono;

vector<float> solve_equation(vector<vector<float>>a, vector<float> b, int n)
{
    vector<float> s(n), x(n);

    for (int i = 0; i < n; i++)
    {
        s.push_back(0);
    }

    x[n-1] = b[n-1]/ a[n-1][0];
    for (int i = n-2; i>= 0; i--)
    {
        for (int j = 0; j < i+1; j++)
        {
            s[j] += a[j][i - j + 1] * x[i+1];
        }
        x[i] = (b[i] - s[i]) / a[i][0];
    }

    return x;
}


int main() {

    vector<float> b;
    int n;

    auto start_read = high_resolution_clock::now();
    n = read_matrix_b("b_input.txt", &b);
    vector<vector<float>> a(n);
    read_matrix_a(&a, n, 10);


    auto start_calc = high_resolution_clock::now(); 
    vector<float> result = solve_equation(a, b, n);
    auto stop = high_resolution_clock::now(); 

    auto duration_full = duration_cast<milliseconds>(stop - start_read);
    auto duration_calc = duration_cast<milliseconds>(stop - start_calc);

    write_into_file("x_output.txt", result);

    cout << "full time " << duration_full.count() << endl;
    cout << "calc time " << duration_calc.count() << endl;

    return 0;
}