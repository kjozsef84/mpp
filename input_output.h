#include <iostream>
#include <vector>
#include <fstream>
#include <omp.h>

using namespace std;

int read_matrix_b(string file_name, vector<float>* b)
{
    int length = 0;
    float value;
    ifstream f(file_name);

    while (f >> value)
    {
        (*b).push_back(value);
        length++;
    }

    f.close();
    return length;
}

void read_matrix_a(string file_name, vector<vector<float>> *a, int n)
{

    int limit = n, index = 0; 
    float value;
    ifstream f(file_name);

    while(1)
    {
        for (int i = 0; i < limit; i++)
        {   
            if (f >> value)
            {
                (*a)[index].push_back(value);
            } else 
            {
                f.close();
                return;
            }
        }
        index++;
        limit--;
    }
}


void read_parallel_a(string file_name, long start_row, long end_row, int n, vector<vector<float>> *a) {


    ifstream f (file_name);
    long nr_item = 0;

    for (int i = 0; i < start_row; i++) {
        nr_item += (n-i);
    }
    nr_item *= 5;
    if (start_row != 0) {
        nr_item += (start_row-1);
    }
    
    f.seekg(nr_item, f.cur);

    for(int i = start_row; i < end_row; i++)
    {
        long aux = n - i;
        for(int j = 0; j < aux; j++)
        {
            float item;
            f >> item;
            (*a)[i].push_back(item);
        }
    }

}

void read_matrix_a(vector<vector<float>>* a, int n, int nr_thread) {
    
    int step = n / nr_thread;
    
    omp_set_num_threads(nr_thread);

    // #pragma omp parallel for schedule (static, 1)
    for (int i = 0; i < n; i+=step) {
        
        read_parallel_a("a_input.txt", i, i + step, n, a);
    }
}


void write_into_file(string file_name, vector<float> result)
{
    ofstream g(file_name);
    
    for (int i = 0; i < result.size(); i++)
    {
        g << result[i] << endl;
    }

    g.close();
}


    // for (int i = 0; i < a.size(); i++)
    // {
    //     for (int j = 0; j < a[i].size(); j++)
    //     {
    //         cout << a[i][j] << " ";
    //     }
    //     cout << endl;
    // }

    // for (vector<float>::iterator it = b.begin(); it < b.end(); it++)
    // {
    //     cout << *it << " ";
    // }
    // cout << endl;

