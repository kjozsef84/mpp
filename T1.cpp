// Solve a linear equation with multithreading 

#include <iostream>
#include <vector>
#include <chrono> 
#include <thread>
#include <mutex>

#include "barrier.cpp"
#include "input_output.h"

using namespace std;
using namespace std::chrono;


vector<float> x;
vector<float> s;



void calc(vector<vector<float>>* a, vector<float>* b, int n, int k, int thread_nr, Barrier* barrier) 
{
    
    // for (int j = k; j < i+1; j+=thread_nr)
    // {
    //     int temp = (*a)[j][i - j + 1] * x[i+1];
    //     s[j] += temp;
    // }
    // x[i] = ((*b)[i] - s[i]) / (*a)[i][0];

    for(int i = n - 2;i >= 0; i--) {
        float aux = ((*b)[i] - s[i]) / (*a)[i][0];
        x[i] = aux;
        int j = k;
        while(j < i+1) {
            s[j] += x[i+1] * (*a)[j][i - j + 1];
            j += thread_nr;
        }
        barrier->wait();
    }

}


// g++ -pthread T1.cpp
int main(int argc, char *argv[]) {

    vector<float> b;
    vector<thread> ths;
    int n, nr_thread;

    auto start_reading = high_resolution_clock::now();
    nr_thread = atoi(argv[1]);
    n = read_matrix_b("b_input.txt", &b);

    for (int i = 0; i < n; i++)
    {
        s.push_back(0);
    }

    vector<vector<float>> a(n);     // fel lehet vinni a tetejere ha resize-olom
    // read_matrix_a("a_input.txt", &a, n);
    
    read_matrix_a(&a, n, nr_thread);

    x.resize(n);
    auto start_calc = high_resolution_clock::now();
    Barrier barrier(nr_thread);
    x[n-1] = b[n-1]/ a[n-1][0];
    // start threads
    for (int k = 0; k < nr_thread; k++)
    {
        ths.push_back(thread(&calc, &a, &b, n, k, nr_thread, &barrier));
    }

    // wait for all the threads to finish
    for (auto& th : ths)
    {
        th.join();
    }
    ths.clear();

    auto stop = high_resolution_clock::now(); 
    auto duration_full = duration_cast<milliseconds>(stop - start_reading);
    auto duration_calc = duration_cast<milliseconds>(stop - start_calc);
    
    write_into_file("x_output.txt", x);

    cout << "full time " << duration_full.count() << endl;
    cout << "calc time " << duration_calc.count() << endl;
 
    return 0;
}
